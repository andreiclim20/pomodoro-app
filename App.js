import React, { Component } from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import { StatusBar, SafeAreaView } from 'react-native'
import { Auth } from 'aws-amplify'

import reducer from './src/redux/reducer'
import mainSaga from './src/sagas/saga'
import Navigator from './src/navigation/Routes'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(mainSaga)

export default class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <StatusBar hidden />
        <Navigator />
      </Provider>
    )
  }
}
