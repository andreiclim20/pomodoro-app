import {
  PUT_USER_ID_TO_GS,
  FETCH_TO_DOS,
  FETCH_TO_DOS_SUCCESS,
  FETCH_TO_DOS_ERROR,
  START_POST_TO_DO,
  POST_TO_DO_SUCCESS,
  POST_TO_DO_ERROR,
  FETCH_DATES,
  FETCH_DATES_SUCCESS,
  FETCH_DATES_ERROR,
  FETCH_TODAY_TODOS,
  FETCH_TODAY_TODOS_SUCCESS,
  FETCH_TODAY_TODOS_ERROR,
  REMOVE_TASK,
  REMOVE_TASK_SUCCESS,
  REMOVE_TASK_ERROR,
  SET_POMODORO_LENGTH,
  SET_POMODORO_LENGTH_SUCCESS,
  SET_POMODORO_LENGTH_ERROR,
  GET_POMODORO_LENGTH_SUCCESS,
  GET_POMODORO_LENGTH_ERROR,
  PUT_TASK_TO_TODAY,
  PUT_TASK_TO_TODAY_SUCCESS,
  PUT_TASK_TO_TODAY_ERROR
} from '../constants/constants'

const initialState = {
  authenticatedUserID: '',
  isFetching: false,
  toDos: {
    error: null,
    list: []
  },
  postToDo: {
    done: false,
    error: null
  },
  dateList: {
    dates: [],
    error: null
  },
  todayTodos: {
    list: [],
    error: null
  },
  removeToDo: {
    done: false,
    error: null
  },
  setPmdLength: {
    done: false,
    error: null
  },
  getPmdLength: {
    done: false,
    error: null
  },
  pomodoroLength: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PUT_USER_ID_TO_GS:
      const { id } = action.payload
      return {
        ...state,
        authenticatedUserID: id
      }
    case FETCH_TO_DOS:
      return {
        ...state,
        isFetching: true,
        toDos: {
          error: null
        }
      }
    case FETCH_TO_DOS_SUCCESS: {
      const { list } = action.payload
      console.log('ListOfToDos[Reducer]', list)
      return {
        ...state,
        isFetching: false,
        toDos: {
          list,
          error: null
        }
      }
    }
    case FETCH_TO_DOS_ERROR:
      return {
        ...state,
        isFetching: false,
        todos: {
          error: action.payload.error
        }
      }
    case START_POST_TO_DO:
      return {
        ...state,
        isFetching: true,
        postToDo: {
          done: false,
          error: null
        }
      }
    case POST_TO_DO_SUCCESS:
      const { updatedList } = action.payload
      console.log('Succesful POST REDUCER', updatedList)
      return {
        ...state,
        isFetching: false,
        postToDo: {
          done: true,
          error: null
        },
        toDos: {
          error: null,
          list: updatedList
        }
      }
    case POST_TO_DO_ERROR:
      console.log('Failed POST REDUCER')
      return {
        ...state,
        isFetching: false,
        postToDo: {
          done: true,
          error: action.payload.error
        }
      }
    case FETCH_DATES:
      console.log('FETCHING DATES...')
      return {
        ...state,
        isFetching: true,
        dateList: {
          error: null
        }
      }
    case FETCH_DATES_SUCCESS:
      console.log('SUCCESSFULLY FETCHED DATES')
      const { dates } = action.payload
      return {
        ...state,
        isFetching: false,
        dateList: {
          dates,
          error: null
        }
      }
    case FETCH_DATES_ERROR:
      console.log('FETCHING DATES FAILED')
      const { error } = action.payload
      return {
        ...state,
        isFetching: false,
        dateList: {
          error
        }
      }
    case FETCH_TODAY_TODOS:
      console.log('FETCHING TODAY TODOS...')
      return {
        ...state,
        isFetching: true,
        todayTodos: {
          error: null
        }
      }
    case FETCH_TODAY_TODOS_SUCCESS: {
      console.log('SUCCESSFULLY FETCHED TODAY TODOS')
      const { list } = action.payload
      return {
        ...state,
        isFetching: false,
        todayTodos: {
          list,
          error: null
        }
      }
    }

    case FETCH_TODAY_TODOS_ERROR:
      console.log('FETCHING TODAY TODOS FAILED')
      return {
        ...state,
        isFetching: false,
        todayTodos: {
          error: action.payload.error
        }
      }
    case REMOVE_TASK:
      return {
        ...state,
        isFetching: true,
        removeToDo: {
          done: false,
          error: null
        }
      }
    case REMOVE_TASK_SUCCESS:
      return {
        ...state,
        isFetching: false,
        removeToDo: {
          done: true,
          error: null
        },
        toDos: {
          error: null,
          list: action.payload.updatedList
        }
      }
    case REMOVE_TASK_ERROR:
      return {
        ...state,
        isFetching: false,
        removeToDo: {
          done: true,
          error: action.payload.error
        }
      }
    case SET_POMODORO_LENGTH:
      console.log('SETTING POMODORO LENGTH...')
      return {
        ...state,
        isFetching: true,
        setPmdLength: {
          error: null
        }
      }
    case SET_POMODORO_LENGTH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        setPmdLength: {
          done: true,
          error: null
        },
      }
    case SET_POMODORO_LENGTH_ERROR:
      return {
        ...state,
        isFetching: false,
        setPmdLength: {
          done: true,
          error: action.payload.error
        }
      }
    case GET_POMODORO_LENGTH_SUCCESS:
      const { newLength } = action.payload
      console.log('GetPomodoroLength Reducer:', newLength)
      return {
        ...state,
        isFetching: false,
        pomodoroLength: newLength,
        getPmdLength: {
          done: true,
          error: null
        }
      }
    case GET_POMODORO_LENGTH_ERROR:
      return {
        ...state,
        isFetching: false,
        getPmdLength: {
          done: true,
          error: action.payload.error
        }
      }
    case PUT_TASK_TO_TODAY:
      return {
        ...state,
        isFetching: true
      }

    case PUT_TASK_TO_TODAY_SUCCESS:
      const { list } = state.toDos
      const { idToDelete } = action.payload
      const toDos = list.filter(toDo => toDo.taskId !== idToDelete)
      return {
        ...state,
        isFetching: false,
        toDos: {
          list: toDos,
          error: null
        }
      }
    case PUT_TASK_TO_TODAY_ERROR:
      return {
        ...state,
        isFetching: false,
        getPmdLength: {
          done: true,
          error: action.payload.error
        }
      }

    default:
      return state
  }
}

export default reducer
