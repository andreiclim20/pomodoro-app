import React from 'react'
import styled from 'styled-components'
import { Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const ReusableInput = (props) => {
  const { placeholder, value, onChangeText, field } = props
  return (
    <StyledInput
      placeholder={placeholder}
      placeholderTextColor='#646464'
      onChangeText={onChangeText(field)}
      value={value}
      autoCapitalize='none'
      autoCorrect={false}
    />
  )
}

const StyledInput = styled.TextInput`
  border-bottom-width: 1;
  border-bottom-color: lightgrey;
  width: ${width * 0.75};
  margin-horizontal: ${width * 0.1};
  margin-vertical: 20;
  padding-vertical: 5;
`

export default ReusableInput
