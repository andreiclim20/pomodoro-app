import React from 'react'
import styled from 'styled-components'
import { Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/Entypo'

const { width } = Dimensions.get('window')

const colorPicker = taskStatus => {
  switch (taskStatus) {
    case 'notStarted':
      return '#4B3F72'
    case 'inProgress':
      return '#AC641C'
    case 'done':
      return '#016831'
    case 'failed':
      return '#7A111E'
    default:
      break
  }
}

const ToDoItem = props => {
  const {
    title,
    pomodoro,
    taskStatus,
    moveIcon,
    onPressMoveIcon,
    removeIcon,
    onPressRemoveIcon,
    startTask,
    onPressStartIcon,
    textColor
  } = props

  return (
    <Wrapper color={colorPicker(taskStatus)}>
      <TodoWrapper>
        <Title>{title}</Title>
        <Estimation>{`${pomodoro} estimated pomodoros`}</Estimation>
      </TodoWrapper>
      <IconsWrapper>
        {moveIcon && (
          <Icon
            onPress={onPressMoveIcon}
            name='arrow-with-circle-right'
            color='white'
            size={30}
          />
        )}
        { startTask && (
          <Icon
            onPress={onPressStartIcon}
            name='controller-play'
            color='white'
            size={30}
          />
        )}
        {removeIcon && (
          <Icon
            onPress={onPressRemoveIcon}
            name='circle-with-cross'
            color='white'
            size={30}
          />
        )}
      </IconsWrapper>
    </Wrapper>
  )
}

const Wrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-self: center;
  width: ${width * 0.9};
  border-radius: 6;
  margin-bottom: 6;
  padding-vertical: 6;
  padding-horizontal: 6;
  text-align: center;
  background-color: ${props => props.color};
`
const TodoWrapper = styled.View`
  align-self: center;
  width: ${width * 0.65};
  padding-horizontal: 6;
`
const Title = styled.Text`
  color: white;
  font-size: 18;
`
const Estimation = styled.Text`
  color: white;
  font-size: 12;
`
const IconsWrapper = styled.View`
  flex-direction: row;
  align-self: center;
`

export default ToDoItem
