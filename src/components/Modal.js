import React from 'react'
import { Dimensions } from 'react-native'
import Modal from 'react-native-modal'
import styled from 'styled-components'

import ReusableInput from './ReusableInput'
import Button from './Button'

const { width, height } = Dimensions.get('window')

const AddTaskModal = props => {
  const { isVisible,
    title,
    estimatedPomodoro,
    handleStateChange,
    createToDo,
    resetFields } = props

  const fields = [
    {
      placeholder: 'What do you want to do?',
      fieldName: 'title',
      value: title
    },
    {
      placeholder: 'Estimated number of pomodoros',
      fieldName: 'estimatedPomodoro',
      value: estimatedPomodoro
    }
  ]

  const toggleModal = () => handleStateChange('modalVisibility')(!isVisible)

  const buttonPress = () => {
    createToDo()
    resetFields()
    toggleModal()
  }

  return (
    <Modal
      transparent={true}
      isVisible={isVisible}
      onBackdropPress={toggleModal}
    >
      <ModalInnerView>
        <CenteredText>Create a task</CenteredText>
        {fields.map(field => (
          <ReusableInput
            key={field.fieldName}
            placeholder={field.placeholder}
            field={field.fieldName}
            value={field.value}
            onChangeText={handleStateChange}
          />
        ))}
        <Button
          background='#443742'
          color='white'
          onPress={buttonPress}
        >
          Save
        </Button>
      </ModalInnerView>
    </Modal>
  )
}

const ModalInnerView = styled.View`
  align-self: center;
  justify-content: center;
  width: ${width};
  height: ${height * 0.5};
  padding-vertical: 10;
  background-color: white;
`
const CenteredText = styled.Text`
  align-self: center;
  font-size: 25;
`

export default AddTaskModal
