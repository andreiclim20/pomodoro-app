import React from 'react'
import styled from 'styled-components'
import { Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const Button = props => {
  const { background, onPress, color, children } = props

  return (
    <Wrapper background={background} onPress={onPress}>
      <Label color={color}>{children}</Label>
    </Wrapper>
  )
}

const Wrapper = styled.TouchableOpacity`
  width: ${width * 0.3};
  background-color: ${props => props.background};
  padding-vertical: 12;
  padding-horizontal: 9;
  align-items: center;
  align-self: center;
  border-radius: 6;
`
const Label = styled.Text`
  color: ${props => props.color};
  font-size: 16;
`

export default Button
