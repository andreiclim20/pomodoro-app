import React from 'react'
import { TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome'

const HeaderButton = props => {
  {
    if (props.name && props.name === 'bars') {
      return <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
        <Icon
          name={props.name}
          size={26}
          style={{ marginLeft: 10 }}
          color={props.color}
        />
      </TouchableOpacity>
    } else if (props.name && props.name === 'chevron-left') {
      return <TouchableOpacity onPress={() => props.navigation.navigate(props.route)}>
        <Icon
          name={props.name}
          size={20}
          style={{ marginLeft: 10 }}
          color={props.color}
        />
      </TouchableOpacity>
    } else if (props.name && props.name === 'plus-circle') {
      return <TouchableOpacity onPress={props.addTask}>
        <Icon
          name={props.name}
          size={26}
          style={{ marginRight: 10 }}
          color={props.color}
        />
      </TouchableOpacity>
    }
  }
}

export default withNavigation(HeaderButton)