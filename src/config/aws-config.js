import Amplify, { Auth } from 'aws-amplify'

export default Amplify.configure({
  Auth: {
    identityPoolId: 'us-east-1:c04b4338-f4da-459e-8205-7503564081ff',
    region: 'us-east-1',
    userPoolId: 'us-east-1_u5FPh3FnL',
    userPoolWebClientId: '795d437b9vt9dvjstpe980s5po'
  },
  API: {
    endpoints: [
      {
        name: 'todos',
        endpoint:
          'https://jxv83zvb0a.execute-api.us-east-1.amazonaws.com/development',
        custom_header: async () => ({
          Authorization: (await Auth.currentSession()).idToken.jwtToken
        })
      }
    ]
  }
})
