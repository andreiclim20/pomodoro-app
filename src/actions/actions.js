import {
  PUT_USER_ID_TO_GS,
  FETCH_TO_DOS,
  FETCH_TO_DOS_SUCCESS,
  FETCH_TO_DOS_ERROR,
  START_POST_TO_DO,
  POST_TO_DO_SUCCESS,
  POST_TO_DO_ERROR,
  FETCH_DATES,
  FETCH_DATES_SUCCESS,
  FETCH_DATES_ERROR,
  FETCH_TODAY_TODOS,
  FETCH_TODAY_TODOS_SUCCESS,
  FETCH_TODAY_TODOS_ERROR,
  PUT_TASK_TO_TODAY,
  PUT_TASK_TO_TODAY_SUCCESS,
  PUT_TASK_TO_TODAY_ERROR,
  REMOVE_TASK,
  REMOVE_TASK_SUCCESS,
  REMOVE_TASK_ERROR,
  SET_POMODORO_LENGTH,
  SET_POMODORO_LENGTH_SUCCESS,
  SET_POMODORO_LENGTH_ERROR,
  GET_POMODORO_LENGTH,
  GET_POMODORO_LENGTH_SUCCESS,
  GET_POMODORO_LENGTH_ERROR
} from '../constants/constants'

export const putUserIdToGs = (id) => ({
  type: PUT_USER_ID_TO_GS,
  payload: { id }
})

export const fetchGetToDos = () => ({
  type: FETCH_TO_DOS
})

export const fetchGetToDosSuccess = list => ({
  type: FETCH_TO_DOS_SUCCESS,
  payload: { list }
})

export const fetchGetToDosError = error => ({
  type: FETCH_TO_DOS_ERROR,
  payload: { error }
})

export const startPostToDo = toDo => ({
  type: START_POST_TO_DO,
  payload: { toDo }
})

export const postToDoSuccess = updatedList => ({
  type: POST_TO_DO_SUCCESS,
  payload: { updatedList }
})

export const postToDoError = error => ({
  type: POST_TO_DO_ERROR,
  payload: { error }
})
export const fetchDates = () => ({
  type: FETCH_DATES
})

export const fetchDatesSuccess = dates => ({
  type: FETCH_DATES_SUCCESS,
  payload: { dates }
})

export const fetchDatesError = error => ({
  type: FETCH_DATES_ERROR,
  payload: { error }
})

export const fetchTodayTodos = date => ({
  type: FETCH_TODAY_TODOS,
  payload: { date }
})

export const fetchTodayTodosSuccess = list => ({
  type: FETCH_TODAY_TODOS_SUCCESS,
  payload: { list }
})

export const fetchTodayTodosError = error => ({
  type: FETCH_TODAY_TODOS_ERROR,
  payload: { error }
})

export const putTaskToToday = taskId => ({
  type: PUT_TASK_TO_TODAY,
  payload: { taskId }
})

export const putTaskToTodaySuccess = idToDelete => ({
  type: PUT_TASK_TO_TODAY_SUCCESS,
  payload: { idToDelete }
})

export const putTaskToTodayError = error => ({
  type: PUT_TASK_TO_TODAY_ERROR,
  payload: { error }
})

export const deleteTask = taskId => ({
  type: REMOVE_TASK,
  payload: { taskId }
})

export const deleteTaskSuccess = updatedList => ({
  type: REMOVE_TASK_SUCCESS,
  payload: { updatedList }
})

export const deleteTaskError = error => ({
  type: REMOVE_TASK_ERROR,
  payload: { error }
})

export const setPomodoroLength = newLength => ({
  type: SET_POMODORO_LENGTH,
  payload: { newLength }
})

export const setPomodoroLengthSuccess = newLength => ({
  type: SET_POMODORO_LENGTH_SUCCESS,
  payload: { newLength }
})

export const setPomodoroLengthError = error => ({
  type: SET_POMODORO_LENGTH_ERROR,
  payload: { error }
})

export const getPomodoroLength = () => ({
  type: GET_POMODORO_LENGTH
})

export const getPomodoroLengthSuccess = newLength => ({
  type: GET_POMODORO_LENGTH_SUCCESS,
  payload: { newLength }
})

export const getPomodoroLengthError = error => ({
  type: GET_POMODORO_LENGTH_ERROR,
  payload: { error }
})
