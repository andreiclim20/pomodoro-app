import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import Timer from 'react-compound-timer'
import { Dimensions, View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux'
import { getPomodoroLength } from '../../actions/actions'

const { width, height } = Dimensions.get('window')

class TimerScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: navigation.getParam('title'),
      headerTintColor: 'white'
    }
  }

  state = {
    countingDown: false,
    reset: false
  }

  componentDidMount () {
    const { getPomodoroLength } = this.props
    getPomodoroLength()
  }
  
  _duration = () => {
    var length = null
    const { navigation, duration } = this.props
    if (navigation.getParam('title') === 'Relax') {
      length = duration.breakLength
    } else {
      length = duration.pomodoroLength
    }
    return length
  }

  render () {
    const { duration } = this.props
    console.log('Duration', duration)
    const { pomodoroLength, breakLength } = duration
    const { countingDown, reset } = this.state
    return (
      <Wrapper>
        <Timer 
          initialTime={this._duration() * 1000 * 60} 
          direction='backward' 
          startImmediately={false}
          onPause={() => this.setState({ reset: false, countingDown: false })}
          onStart={() => this.setState({ reset: false, countingDown: true })}
          onReset={() => this.setState({ countingDown: false, reset: true })}
          lastUnit='m'>
          {({ start, pause, reset }) => (
            <ContentWrapper>
              <View>
                <Countdown>
                   <CountdownLabel> <Minutes /> minutes </CountdownLabel>
                   <CountdownLabel> <Seconds /> seconds </CountdownLabel>
                </Countdown>
              </View>

              <ButtonsWrapper>
                <Button>
                  <Icon size={60} color='white' name='play-circle-outline' onPress={start} />
                </Button>
                <Button>
                  <Icon size={60} color='white' name='pause-circle-outline' onPress={pause} />
                </Button>
                <Button>
                  <Icon size={60} color='white' name='refresh' onPress={reset} />
                </Button>
              </ButtonsWrapper>
            </ContentWrapper>
          )}
        </Timer>
      </Wrapper>
    )
  }
}

const Wrapper = styled.View`
  width: ${width};
  height: ${height};
  align-items: center;
  justify-content: center;
  background-color: #443742;
`
const ContentWrapper = styled.View`
  height: ${height * 0.5};
  justify-content: space-around;
  align-items: center;
`
const Button = styled.TouchableOpacity`
`
const ButtonsWrapper = styled.View`
  flex-direction: row;
  width: ${width * 0.9};
  justify-content: space-around;
`
const Countdown = styled.View`
flex-direction: column;
`
const CountdownLabel = styled.Text`
  text-align: center;
  font-size: 30;
  color: white;
`
const MinutesWrapper = styled.View`
`
const Label = styled.Text`
`
const Minutes = styled(Timer.Minutes)`
`
const Seconds = styled(Timer.Seconds)`
`

const mapStateToProps = state => ({
  duration: state.pomodoroLength
})

const mapDispatchToProps = {
  getPomodoroLength
}

export default connect(mapStateToProps, mapDispatchToProps)(TimerScreen)