import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { ScrollView, Dimensions, TouchableOpacity } from 'react-native'
import Modal from 'react-native-modal'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { fetchGetToDos, startPostToDo, putTaskToToday, deleteTask } from '../../actions/actions'
import AddTaskModal from '../../components/Modal'
import ToDoItem from '../../components/ToDoItem'
import HeaderButton from '../../components/HeaderButton'
import Button from '../../components/Button'

const { width, height } = Dimensions.get('window')

class ActivitySheet extends Component {
  static navigationOptions = {
    title: 'Activity Sheet',
    headerLeft: <HeaderButton color='white' name='bars' />,
    headerStyle: { backgroundColor: '#443742' },
    headerTintColor: 'white',
  }

  state = {
    modalVisibility: false,
    title: '',
    estimatedPomodoro: '',
    showDeleteModal: false,
    idToDelete: null,
    titleToDelete: null,
    a: 1
  }

  componentDidMount () {
    const { fetchGetToDos } = this.props
    fetchGetToDos()
  }

  handleStateChange = key => value => {
    this.setState({ 
      [key]: value
     })
  }

  resetState = () => {
    this.setState({
        modalVisibility: false,
        title: '',
        estimatedPomodoro: ''
    })
  }

  toggleModal = () => this.setState({modalVisibility: !this.state.modalVisibility})

  createToDo = () => {
    const { startPostToDo } = this.props
    const { title, estimatedPomodoro } = this.state
    const estimatedNumber = Number(estimatedPomodoro)
    
    startPostToDo({
      title,
      estimatedPomodoro: estimatedNumber
    })
  }

  moveTaskToToday = id => () => {
    console.log('idMoove:', id)
    const { putTaskToToday } = this.props
    putTaskToToday(id)
  }

  onPressRemove = (id, title) => () => {
    this.setState({
      ...this.state,
      showDeleteModal: true,
      idToDelete: id,
      titleToDelete: title
    })
  }

  onConfirmDelete = id => () => {
    const { deleteTask } = this.props
    deleteTask(id)
    this.setState({showDeleteModal: false})
  }

  onCancelDelete = () => this.setState({showDeleteModal: false})

  render() {
    const { modalVisibility, title, estimatedPomodoro, showDeleteModal, idToDelete, titleToDelete } = this.state
    const { toDos, isFetching } = this.props
    console.log('Render ActivitySheet Todos:', toDos)
    if (isFetching) {
      return <LoadingTransition>
        <LoadingText>One moment please</LoadingText>
      </LoadingTransition>
    }

    return (
      <ActivitySheetWrapper>
          <Header>
            <Title>My Tasks</Title>
            <TouchableOpacity onPress={this.toggleModal}>
              <Icon name='plus' size={30} color='black' />
            </TouchableOpacity>
          </Header>
        <ScrollableView>
          <ScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
          >
            { modalVisibility && <AddTaskModal
              isVisible={modalVisibility}
              title={title}
              estimatedPomodoro={estimatedPomodoro}
              handleStateChange={this.handleStateChange}
              createToDo={this.createToDo}
              resetFields={this.resetState}
              /> 
            }
            {
              showDeleteModal && <Modal
                transparent={true}
                isVisible={showDeleteModal}
                onBackdropPress={ () => this.setState({showDeleteModal: !showDeleteModal})}
              >
                <DeleteModalWrapper>
                  <DeleteModalText>
                    Are you sure that you want to delete following task: {titleToDelete}?
                  </DeleteModalText>
                  <DeleteButtonsWrapper>
                    <Button
                      background='green'
                      onPress={this.onConfirmDelete(idToDelete)}
                      color='white'
                    >
                      Confirm
                    </Button>
                    <Button
                      background='red'
                      onPress={this.onCancelDelete}
                      color='white'
                    >
                      Cancel
                    </Button>
                  </DeleteButtonsWrapper>
                </DeleteModalWrapper>
              </Modal>
            }
            {
              toDos && toDos.map((toDo, index) => {
                const {taskId, title, estimatedPomodoro} = toDo
                  return (
                  <ToDoItem
                    key={taskId}
                    title={title}
                    pomodoro={estimatedPomodoro}
                    taskStatus='notStarted'
                    moveIcon
                    onPressMoveIcon={this.moveTaskToToday(taskId)}
                    removeIcon
                    onPressRemoveIcon={this.onPressRemove(taskId, title)}
                />)
              })
            }
          </ScrollView>
        </ScrollableView>
      </ActivitySheetWrapper>
    );
  }
}

const ActivitySheetWrapper = styled.View`
  width: ${width};
  height: ${height};
  background-color: #CEA07E;
  padding-vertical: 12;
`
const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-horizontal: ${width * 0.075};
  padding-vertical: 12;
  width: ${width};
`
const Title = styled.Text`
  font-size: 20;
  color: black;
`
const DeleteModalWrapper = styled.View`
  align-self: center;
  justify-content: center;
  width: ${width};
  background-color: transparent;
`
const DeleteButtonsWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding-horizontal: 50;
  padding-top: 15;
`
const DeleteModalText = styled.Text`
  text-align: center;
  align-self: center;
  font-size: 25;
  color: white;
`
const ScrollableView = styled.View`
  flex-direction: column;
  height: ${height * 0.8};
  width: ${width};
`
const LoadingTransition = styled.View`
  width: ${width};
  height: ${height};
  justify-content: center;
  align-items: center;
  background-color: #CEA07E;
`
const LoadingText = styled.Text`
  font-size: 20;
  color: black;
`
const mapStateToProps = state => ({
  toDos: state.toDos.list,
  isFetching: state.isFetching
})

const mapDispatchToProps = {
  fetchGetToDos,
  startPostToDo,
  putTaskToToday,
  deleteTask
}

export default connect(mapStateToProps, mapDispatchToProps)(ActivitySheet)
