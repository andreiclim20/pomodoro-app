import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { Dimensions, Picker } from 'react-native'

import Button from '../../components/Button'
import { setPomodoroLength } from '../../actions/actions'

const pomodoroIcon = require('../../assets/counter.png')
const breakIcon = require('../../assets/break-1.png')

const { width, height } = Dimensions.get('window')

class SettingsScreen extends Component {
  state = {
    pmdLength: null,
    selectedPomodoroLength: null,
    selectedBreakLength: null,
    saved: false
  }

  handleStateChange = key => value => {
    this.setState({
      [key]: value
    })
  }

  onPressSet = () => {
    const { setPomodoroLength } = this.props
    const { selectedPomodoroLength, selectedBreakLength } = this.state
    let NewLengths = {}
    if(selectedPomodoroLength) {
      NewLengths.pomodoroLength = selectedPomodoroLength
    }
    if(selectedBreakLength) {
      NewLengths.breakLength = selectedBreakLength
    }
    if(Object.keys(NewLengths).length !== 0) {
      setPomodoroLength(NewLengths)
    }
    this.setState({ ...this.state, saved: true })
    setTimeout(() => this.setState({ ...this.state, saved: false }), 1000)
  }

  render () {
    const { pmdLength, selectedPomodoroLength, selectedBreakLength, enabledPomodoroPicker } = this.state
    const { pomodoroLength } = this.props

    return (
      <Content>
        <Title>Customize your pomodoro length</Title>
        <PickersWrapper>
          <Column>
            <ColumnImage
              source={pomodoroIcon}
            />
            <Picker
              selectedValue={selectedPomodoroLength || pomodoroLength.pomodoroLength}
              onValueChange={(itemValue, itemIndex) =>
                this.handleStateChange('selectedPomodoroLength')(itemValue)
              }
            >
              <Picker.Item label='25' value={25} color='black' />
              <Picker.Item label='30' value={30} color='black' />
              <Picker.Item label='45' value={45} color='black' />
              <Picker.Item label='60' value={60} color='black' />
            </Picker>
          </Column>
          <Column>
            <ColumnImage
              source={breakIcon}
            />
            <Picker
              selectedValue={selectedBreakLength || pomodoroLength.breakLength}
              onValueChange={(itemValue, itemIndex) =>
                this.handleStateChange('selectedBreakLength')(itemValue)
              }
            >
              <Picker.Item label='5' value={5} color='black' />
              <Picker.Item label='10' value={10} color='black' />
              <Picker.Item label='15' value={15} color='black' />
              <Picker.Item label='20' value={20} color='black' />
            </Picker>
          </Column>
        </PickersWrapper>
        <Button background='#443742' onPress={this.onPressSet} color='white'>
          {this.state.saved ? 'Saved!' : 'Save'}
        </Button>
      </Content>
    )
  }
}

const Content = styled.View`
  align-items: center;
  justify-content: center;
  width: ${width};
  height: ${height};
  background-color: #CEA07E;
`
const Title = styled.Text`
  font-size: 22;
  text-align: center;
  padding-horizontal: 6;
  padding-vertical: 6;
  color: black;
`
const PickersWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: ${width};
  margin-vertical: 10;
  padding-horizontal: 10;
`
const Column = styled.View`
  width: ${width * 0.4};
`
const ColumnImage = styled.Image`
  align-self: center;
  width: 100;
  height: 100;
`
const mapStateToProps = state => ({
  pomodoroLength: state.pomodoroLength
})
const mapDispatchToProps = {
  setPomodoroLength
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen)
