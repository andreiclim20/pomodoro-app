import React, { Component } from 'react'
import styled from 'styled-components'
import { Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

class ProfileScreen extends Component {
  static navigationOptions = {
    title: 'Profile'
  }

  render () {
    return (
      <Wrapper>
        <Title>Profile Screen Content</Title>
      </Wrapper>
    )
  }
}

const Wrapper = styled.View`
  align-items: center;
  justify-content: center;
  width: ${width};
  height: ${height};
  background-color: #CEA07E;
`
const Title = styled.Text`
  font-size: 30;
  color: white;
`

export default ProfileScreen