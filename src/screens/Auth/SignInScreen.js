import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Auth } from 'aws-amplify'
import { Dimensions } from 'react-native'
import { withNavigation } from 'react-navigation'
import styled from 'styled-components'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { putUserIdToGs } from '../../actions/actions'
import Button from '../../components/Button'

const { width, height } = Dimensions.get('window')

class SignInScreen extends Component {
  state = {
    username: '',
    password: ''
  }

  _handleStateChange = (authState, params = null) => () => {
    const { onStateChange } = this.props
    onStateChange(authState, params)
  }

  _logIn = () => {
    const { username, password } = this.state
    const { navigation, authData } = this.props
    const user = (authData && authData.username) || username

    Auth.signIn(user, password)
      .then(user => {
        const { putUserIdToGs } = this.props
        console.log('Success login', user)
        putUserIdToGs(user.username)
        navigation.navigate('Home')
      })
      .catch(err => {
        console.log('* Error caught in _signIn', err)
        console.log('err.code:', err.code)
        if (err.code.includes('UserNotConfirmedException')) {
          console.log('ERROR IN LOG IN', err)
          this._handleStateChange('confirmSignUp', {
            backState: 'signIn',
            username
          })()
        } else if (err.code.includes('UserNotFoundException')) {
          this._handleStateChange('signUp', {
            backState: 'signIn',
            username
          })()
        }
      })
    }

  _goBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    const { authState, authData } = this.props
    if (!['signIn', 'signedOut', 'signedUp'].includes(authState)) {
      return null
    }

    return (
      <Wrapper>
        <IconWrapper onPress={this._goBack}>
          <Icon name='chevron-left' size={30} color='black'/>
        </IconWrapper>
        <FormSection>
          <Title>Sign In To Your Account</Title>
          <Label
            placeholder='Enter Your Email'
            placeholderTextColor='#646464'
            onChangeText={(username) => this.setState({username})}
            value={this.state.username}
            autoCapitalize='none'
            autoCorrect={false}
          />
          <Label
            placeholder='Enter your password'
            placeholderTextColor='#646464'
            secureTextEntry
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
            autoCapitalize='none'
            autoCorrect={false}
          />
          <Button 
            color='white' 
            background='#119DA4'
            onPress={this._logIn}>Sign In
          </Button>

        </FormSection>
      </Wrapper>
    )
  }
}

const Wrapper = styled.View`
  width: ${width};
  height: ${height * 1.25};
  justify-content: center;
  background-color: #EDD9A3;
`
const Title = styled.Text`
  align-self: center;
  margin-bottom: 5;
  font-size: 20;
`
const IconWrapper = styled.TouchableOpacity`
  height: ${height * 0.1};
  justify-content: center;
  padding-horizontal: 6;
`
const FormSection = styled.View`
  height: ${height * 0.9};
  justify-content: center;
`
const LabelTitle = styled.Text`
  font-size: 25;
  color: blue;
  margin-bottom: 3;
`
const Label = styled.TextInput`
  border-bottom-width: 1;
  border-bottom-color: grey;
  width: ${width * 0.75};
  margin-horizontal: ${width * 0.1};
  margin-vertical: 20;
  padding-vertical: 5;
`

const mapDispatchToProps = {
  putUserIdToGs
}

export default connect(null, mapDispatchToProps)(withNavigation(SignInScreen))
