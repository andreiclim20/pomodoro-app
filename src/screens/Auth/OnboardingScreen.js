import React, { Component } from 'react'
import styled from 'styled-components'
import { Dimensions, Image } from 'react-native'
import { Auth } from 'aws-amplify'

import Button from '../../components/Button'

const { width, height } = Dimensions.get('window')
const pomodoro = require('../../assets/counter.png')

class OnboardingScreen extends Component {
  componentDidMount () {
    Auth.currentAuthenticatedUser()
      .then(response => console.log('RESPONSE ONBOARDING USER', response))
  }

  _navigateTo = (route, params) => () => {
    const { navigate } = this.props.navigation
    navigate(route, params)
  }

  _handleSignOut = () => {
    Auth.signOut()
      .then(response => {
        console.log('User logged out successfully')
        this.props.navigation.navigate('Onboarding')
      })
      .catch(err => console.log('Error in sign out', err))
  }

  render () {
    return (
      <Wrapper>
        <TitleWrapper>
          <Heading>Pomo d'Oro</Heading>
          <Subheading>The golden apple of productivity apps</Subheading>
        </TitleWrapper>
        <Image source={pomodoro} style={{alignSelf: 'center'}}/>
        <ButtonSection>
          <Button 
            onPress={this._navigateTo('Auth', { authState: 'signUp'} )} 
            background='#443742' 
            color='white'>
            Join Now
          </Button>
          <Button 
            onPress={this._navigateTo('Auth', { authState: 'signIn'} )} 
            background='#119DA4' 
            color='white'>
            Sign In
          </Button>

          <Button 
            onPress={this._handleSignOut} 
            background='#443742' 
            color='white'>Log out</Button> 

        </ButtonSection>
        <ForgotPassword>
          <Caption>Forgot Password?</Caption>
        </ForgotPassword>
      </Wrapper>
    )
  }
}

const Wrapper = styled.View`
  width: ${width};
  height: ${height};
  background-color: #EDD9A3;
  justify-content: space-around;
`
const TitleWrapper = styled.View`
  justify-content: flex-end;
`
const Heading = styled.Text`
  align-self: center;
  margin-top: ${height * 0.05};
  font-size: 40;
`
const Subheading = styled.Text`
  align-self: center;
  margin-top: ${height * 0.05};
  font-size: 20;
`
const ButtonSection = styled.View`
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`
const Caption = styled.Text`
  margin-vertical: 20;
  align-self: center;
`
const ForgotPassword = styled.TouchableOpacity`
`

export default OnboardingScreen