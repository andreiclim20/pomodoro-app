import React, { Component } from 'react'
import styled from 'styled-components'
import { Dimensions, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Auth } from 'aws-amplify'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import HeaderButton from '../../components/HeaderButton'
import Button from '../../components/Button'

const { width, height } = Dimensions.get('window')

class SignUpScreen extends Component {
  state = {
    username: '',
    password: '',
    email: '',
    firstName: '',
    error: null
  }

  _handleTextChange = key => value => {
    this.setState({
      [key]: value,
      error: null
    })
  }

  _handleStateChange = (authState, params = null) => () => {
    const { onStateChange } = this.props
    onStateChange(authState, params)
  }

  _signUp = () => {
    const { email, firstName, password, username } = this.state
    const { authData } = this.props
    const user = (authData && authData.username) || username
    Auth.signUp({
      username: user,
      password,
      attributes: {
        email: user,
        'custom:firstName': firstName
      }
    }).then(data => {
      this._handleStateChange('signIn', { username: user })()
      console.log('Sign up successful')

    }).catch(err => console.log('Error caught in Sign up:', err))
  }

  _goBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    const { authData, authState } = this.props
    const { username, password, email, firstName, error } = this.state
    if (authState !== 'signUp') {
      return null
    }
    const user = (authData && authData.username || username)
    return (
      <Wrapper>
        <IconWrapper onPress={this._goBack}>
          <Icon name='chevron-left' size={30} color='black'/>
        </IconWrapper>
          <FormSection>
          <Title>Create New Account</Title>
            <Field 
              placeholder='Enter Email To Begin' 
              placeholderTextColor='#646464'
              onChangeText={this._handleTextChange('username')}
              defaultValue={user}
              autoCapitalize='none'
            />
            <Field 
              placeholder='Create Password' 
              placeholderTextColor='#646464'
              onChangeText={this._handleTextChange('password')}
              defaultValue={password}
              secureTextEntry
              autoCapitalize='none'
            />
            <Field 
              placeholder='Enter Your Wanted Display Name'
              placeholderTextColor='#646464'
              onChangeText={this._handleTextChange('firstName')}
              defaultValue={firstName}
              autoCapitalize='none'
            />
            <Button 
              onPress={this._signUp} 
              background='#3CAEB4' 
              color='white'>
              Sign Up
            </Button>
          </FormSection>
      </Wrapper>
    )
  }
}

const Wrapper = styled.View`
  width: ${width};
  height: ${height * 1.1};
  justify-content: space-around;
  background-color: #EDD9A3;
`
const Title = styled.Text`
  margin-vertical: 20;
  font-size: 20;
  text-align: center;
`
const IconWrapper = styled.TouchableOpacity`
  height: ${height * 0.1};
  justify-content: center;
  padding-horizontal: 6;
`
const FormSection = styled.View`
  height: ${height * 0.9};
  justify-content: center;
`
const Field = styled.TextInput`
  border-bottom-width: 1;
  border-bottom-color: grey;
  width: ${width * 0.75};
  margin-horizontal: ${width * 0.1};
  margin-vertical: 20;
  padding-vertical: 5;
`
export default withNavigation(SignUpScreen)