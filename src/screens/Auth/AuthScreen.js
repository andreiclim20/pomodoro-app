import React, { Component } from 'react'
import { Text } from 'react-native'
import styled from 'styled-components'
import { Authenticator } from 'aws-amplify-react-native'
import { withNavigation } from 'react-navigation'

import SignInScreen from './SignInScreen'
import SignUpScreen from './SignUpScreen'

class AuthScreen extends Component {
  render () {
    const { params: { authState } = {} } = this.props.navigation.state
    console.log('AUTH STATE', authState)
    return (
      <Authenticator
        hideDefault
        authState={authState}
      >
        <SignUpScreen />
        <SignInScreen />
      </Authenticator>
    )
  }
}

export default withNavigation(AuthScreen)