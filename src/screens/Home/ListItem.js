import React from 'react'
import styled from 'styled-components'
import { Dimensions, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { withNavigation } from 'react-navigation'

const { width } = Dimensions.get('window')

const ListItem = props => {
  return (
    <Wrapper>
      <TodoWrapper>
        <Title>{props.title}</Title>
        <Estimation>{props.pomodoros + ' estimated pomodoros'}</Estimation>
      </TodoWrapper>
      <ButtonsWrapper>
        {
          props.startTask && <TouchableOpacity onPress={() => props.navigation.navigate('Timer')}>
            <Icon
              name='play'
              size={30}
              color='grey'
            />
          </TouchableOpacity>
        }
      </ButtonsWrapper>
    </Wrapper>
  )
}

const Wrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: ${width * 0.8};
  border-color: lightgrey;
  border-width: 1;
  margin-bottom: 6;
  padding-vertical: 6;
  padding-horizontal: 6;
  text-align: center;
`
const TodoWrapper = styled.View`
`
const ButtonsWrapper = styled.View`
`
const Title = styled.Text`
  color: black;
  font-size: 18;
`
const Estimation = styled.Text`
  color: black;
  font-size: 14;
`
export default withNavigation(ListItem)