import React, { Component, Fragment } from 'react'
import { Text, Dimensions, ScrollView } from 'react-native'
import { Auth } from 'aws-amplify'
import styled from 'styled-components'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux'

import ListItem from '../../../src/components/ToDoItem'
import Button from '../../components/Button'
import { fetchTodayTodos, getPomodoroLength } from '../../actions/actions'

const { width, height } = Dimensions.get('window')

class HomeScreen extends Component {
  componentDidMount () {
    const { fetchTodayTodos, getPomodoroLength } = this.props
    fetchTodayTodos()
    getPomodoroLength()  
  }

  _navigateTo = (route, params) => () => {
    this.props.navigation.navigate(route, params)
  }

  _renderContent () {
    const { tasks, isFetching } = this.props
    if (isFetching) {
      return (
        <LoadingTransition>
          <LoadingText>One moment please</LoadingText>
        </LoadingTransition>
      )
    }
    if (tasks && tasks.length > 0) {
      return (
        <Wrapper>
            <Heading>Your tasks for today</Heading>
            <ScrollView>
            <ListWrapper>
              {tasks.map(task => (
                <ListItem 
                  key={task.taskId} 
                  title={task.title} 
                  pomodoro={task.estimatedPomodoro} 
                  onPressStartIcon={this._navigateTo('Timer', { title: task.title, duration: 25 })} 
                  textColor='black' 
                  iconColor='white'
                  taskStatus='notStarted' 
                  startTask
                />
              ))}
            </ListWrapper>
          </ScrollView>
        </Wrapper>
      )
    } else {
      return (
        <Wrapper>
          <Heading>You have no tasks assigned for today</Heading>
          <SelectSection onPress={this._navigateTo('ActivitySheet')}>
            <SelectText>Go ahead and assign some to get started!</SelectText>
          </SelectSection>
        </Wrapper>
      )
    }
  }

  render () {
    return (
      <Fragment>
        {this._renderContent()}
        <ControlSection>
          <Button 
            background='#D1A448' 
            color='white'
            onPress={this._navigateTo('Timer', { title: 'Focus', duration: 25 })}>
            Focus
          </Button>
          <Button 
            background='#119DA4' 
            color='white'
            onPress={this._navigateTo('Timer', { title: 'Relax', duration: 5 })}>
            Relax
          </Button>
        </ControlSection>
      </Fragment>
    )
  }
}

const Wrapper = styled.View`
  justify-content: center;
  align-items: center;
  height: ${height * 0.8};
  background-color: #CEA07E;
`
const Heading = styled.Text`
  font-size: 20;
  margin-vertical: 20;
  text-align: center;
  color: black;
`
const ListWrapper = styled.View`
  width: ${width};
  align-items: center;
`
const LoadingTransition = styled.View`
  width: ${width};
  height: ${height};
  justify-content: center;
  align-items: center;
  background-color: #CEA07E;
`
const LoadingText = styled.Text`
  font-size: 20;
  color: black;
`
const ControlSection = styled.View`
  border-top-color: grey;
  border-top-width: 1;
  flex-direction: row;
  justify-content: space-around;
  background-color: #443742;
  height: ${height * 0.11};
`
const SelectSection = styled.TouchableOpacity`
  height: ${height * 0.25};
  width: ${height * 0.25};
  background-color: #119DA4;
  align-items: center;
  justify-content: center;
  border-radius: 100;
  padding-horizontal: 12;
`
const SelectText = styled.Text`
  text-align: center;
  color: white;
  font-size: 16;
`

const mapDispatchToProps = {
  fetchTodayTodos,
  getPomodoroLength
}

const mapStateToProps = state => ({
  tasks: state.todayTodos.list,
  isFetching: state.isFetching
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
