import React from 'react'
import { 
  createSwitchNavigator, 
  createDrawerNavigator, 
  createStackNavigator, 
  createAppContainer 
} from 'react-navigation'
import { View, Text, Dimensions, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import OnboardingScreen from '../screens/Auth/OnboardingScreen'
import AuthScreen from '../screens/Auth/AuthScreen'

import HomeScreen from '../screens/Home/HomeScreen'
import ProfileScreen from '../screens/Profile/ProfileScreen'
import ActivitySheetScreen from '../screens/ActivitySheet/ActivitySheetScreen'
import SettingsScreen from '../screens/Settings/SettingsScreen'

import TimerScreen from '../screens/Timer/TimerScreen'

import SideDrawerContent from './SideDrawerContent'
import HeaderButton from '../components/HeaderButton'

const Guest = createStackNavigator(
  {
    Auth: AuthScreen,
    Onboarding: OnboardingScreen,
  }, 
  {
    headerMode: 'none',
    initialRouteName: 'Onboarding'
  }
)

const Home = createStackNavigator(
  {
    Home: { 
      screen: HomeScreen,
      navigationOptions: ({navigation}) => ({
        title: 'Home',
        headerLeft: <HeaderButton color='white' name='bars' />,
        headerMode: 'float',
        headerStyle: { backgroundColor: '#443742' },
        headerTintColor: 'white',
      })
    },
    Timer: {
      screen: TimerScreen,
      navigationOptions: ({navigation}) => ({
        headerLeft: <HeaderButton color='white' name='chevron-left' route='Home' />,
        headerStyle: { backgroundColor: '#443742' },
        headerTintColor: 'white',
      })
    },
  }, {
    initialRouteName: 'Home'
  }
)

const ActivitySheet = createStackNavigator({
  ActivitySheet: {
    screen: ActivitySheetScreen
  }
})

const Settings = createStackNavigator({
  Settings: {
    screen: SettingsScreen,
    navigationOptions: {
      title: 'Settings',
      headerLeft: <HeaderButton color='white' name='bars' />,
      headerStyle: { backgroundColor: '#443742' },
      headerTintColor: 'white',
    }
  }
})

const Profile = createStackNavigator({
  Profile: {
    screen: ProfileScreen,
    navigationOptions: {
      title: 'Profile',
      headerLeft: <HeaderButton color='white' name='bars' />,
      headerStyle: { backgroundColor: '#443742' },
      headerTintColor: 'white',
    }
  }
})

const DrawerNavigation = createDrawerNavigator({
  Home,
  ActivitySheet,
  Settings,
  Profile
},
  {
    contentComponent: SideDrawerContent,
    drawerWidth: 250
  }
)

const RootNav = createSwitchNavigator(
  {
    Guest,
    DrawerNavigation,
  },
  {
    initialRouteName: 'Guest'
  }
)

const AppContainer = createAppContainer(RootNav)

export default AppContainer
