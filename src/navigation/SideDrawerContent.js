import React, { Component } from 'react'
import { View, ScrollView, Dimensions, StyleSheet, AsyncStorage } from 'react-native'
import styled from 'styled-components'
import { Auth } from 'aws-amplify'
import { withNavigation } from 'react-navigation'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const { width, height } = Dimensions.get('window')

const routes = ['Home', 'Activity Sheet', 'Settings']
const iconNames = ['home', 'file-document-box', 'folder-edit']

class SideDrawerContent extends Component {
  _handleSignOut = () => {
    Auth.signOut()
      .then(response => {
        console.log('User logged out successfully')
        this.props.navigation.navigate('Onboarding')
      })
      .catch(err => console.log('Error in sign out', err))
  }

  _removeItem = async item => {
    try {
      AsyncStorage.removeItem(item)
    } catch (err) {
      console.log('Error caught in [DRAWER SIGN OUT] Remove Item:', err)
    }
  }

  _navigateTo = route => () => {
    this.props.navigation.navigate(route)
  }

  render () {
    return (
      <Wrapper>
        <ScrollView>
          {routes.map((route, index) => (
            <Item 
              key={index} 
              onPress={this._navigateTo(route.split(' ').join(''))}
            >
              <Icon 
                style={{paddingRight: 12, paddingLeftt: 6}} 
                size={20} 
                name={iconNames[index]} 
              />
              <Title>{route}</Title>
            </Item>
          ))}
          <Item onPress={this._handleSignOut}>
            <Icon 
              style={{paddingRight: 12, paddingLeftt: 6}} 
              size={20} 
              name='logout-variant' 
            />
            <Title>Sign Out</Title>
          </Item>
        </ScrollView>
      </Wrapper>
    )
  }
}

const styles = StyleSheet.create({
  scrollview: {
    marginTop: 50
  }
})

const Wrapper = styled.View`
  margin-top: 80;
`
const Item = styled.TouchableOpacity`
  margin-vertical: 10;
  margin-left: 10;
  border-bottom-width: 1;
  border-bottom-color: lightgrey;
  width: ${width * 0.6};
  padding-bottom: 6;
  flex-direction: row;
`
const Title = styled.Text`
  font-size: 16;
`

export default withNavigation(SideDrawerContent)