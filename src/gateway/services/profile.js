const { callEndpoint } = require('./helpers/callEndpoints')

const PROFILE_WS_URL = process.env.PROFILE_WS_URL

const registerRoutes = router => {
  router.get('/settings', async (req, res) => {
    const response = await callEndpoint(`${PROFILE_WS_URL}/settings`, 'get', req.headers.user)
    res.status(response.status).json(response.data)
  })

  router.put('/settings', async (req, res) => {
    const response = await callEndpoint(`${PROFILE_WS_URL}/settings`, 'put', req.headers.user, req.body)
    res.status(response.status).end()
  })
}

module.exports.default = { registerRoutes }
