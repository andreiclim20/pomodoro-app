const { callEndpoint } = require('./helpers/callEndpoints.js')

const DATES_WS_URL = process.env.DATES_WS_URL

const date = date => date ? `/dates/${date}` : '/dates'

const registerRoutes = router => {
  router.get(date(':date'), async (req, res) => {
    const response = await callEndpoint(`${DATES_WS_URL}/dates/${req.params.date}`, 'get', req.headers.user)
    res.status(response.status).json(response.data)
  })

  router.put(`${date(':date')}/task`, async (req, res) => {
    const response = await callEndpoint(`${DATES_WS_URL}/dates/${req.params.date}/task`, 'put', req.headers.user, req.body)
    res.status(response.status).json(response.data)
  })

  router.patch(date(':date'), async (req, res) => {
    const response = await callEndpoint(`${DATES_WS_URL}/dates/${req.params.date}`, 'put', req.headers.user, req.body)
    res.status(response.status).json(response.data)
  })
}

module.exports.default = { registerRoutes }
