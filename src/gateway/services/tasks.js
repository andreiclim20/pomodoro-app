const { callEndpoint } = require('./helpers/callEndpoints.js')

const TASKS_WS_URL = process.env.TASKS_WS_URL

const tasks = id => id ? `/tasks/${id}` : '/tasks'

const registerRoutes = router => {
  router.get(tasks(), async (req, res) => {
    const response = await callEndpoint(`${TASKS_WS_URL}/tasks`, 'get', req.headers.user)
    res.status(response.status).json(response.data)
  })

  router.post(tasks(), async (req, res) => {
    const response = await callEndpoint(`${TASKS_WS_URL}/tasks`, 'post', req.headers.user, req.body)
    res.status(response.status).json(response.data)
  })
  router.delete(tasks(':id'), async (req, res) => {
    const response = await callEndpoint(`${TASKS_WS_URL}/tasks/${req.params.id}`, 'delete', req.headers.user)
    res.status(response.status).json(response.data)
  })
  router.get(tasks(':id'), async (req, res) => {
    const response = callEndpoint(`${TASKS_WS_URL}/tasks/${req.params.id}`, 'get', req.headers.user)
    res.status(response.status).json(response.data)
  })
  router.put(tasks(':id'), async (req, res) => {
    const response = callEndpoint(`${TASKS_WS_URL}/tasks/${req.params.id}`, 'put', req.headers.user, req.body)
    res.status(response.status).json(response.data)
  })
}

module.exports.default = { registerRoutes }
