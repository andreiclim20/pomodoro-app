const axios = require('axios')

exports.callEndpoint = async (url, method, userId, data) => {
  console.log(`USER ${userId} IS CALLING ${url} WITH ${method} METHOD.`)
  try {
    checkUser(userId)
    const params = data
      ? {
        config: { headers: { userId } },
        data: { ...data }
      }
      : { headers: { userId } }
    const response = data
      ? await axios[method](url, params.data, params.config)
      : await axios[method](url, params)

    return {
      status: response.status,
      data: response.data
    }
  } catch (err) {
    console.log('ERROR', err.response.status, err.response.data)
    return {
      status: err.response.status,
      data: err.response.data
    }
  }
}

const checkUser = userId => {
  if (!userId) {
    throw new UserError(403, 'User must be specified in header')
  }
}

class UserError extends Error {
  constructor (status, data) {
    super()
    this.response = { status, data }
  }
}
