const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
const tasks = require('./services/tasks').default
const dates = require('./services/dates').default
const profile = require('./services/profile').default
const app = express()
const router = express.Router()

router.use(cors())
router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))
router.use(awsServerlessExpressMiddleware.eventContext())

dates.registerRoutes(router)
tasks.registerRoutes(router)
profile.registerRoutes(router)

app.use('/', router)

module.exports = app
