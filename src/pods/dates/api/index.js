const httpResponse = require('./src/web/httpResponse')
const repo = require('./src/domain/repositories/dates')
const tasksWS = require('./src/web/tasksWebService')

const getDateDetails = require('./src/domain/usecases/getDateDetails').default
const updateDate = require('./src/domain/usecases/updateDate').default
const addTaskToDate = require('./src/domain/usecases/addTaskToDate').default
const getDates = require('./src/domain/usecases/getDates').default

exports.getDateDetails = async event => {
  try {
    const date = event.pathParameters.date
    const userId = event.headers.userId

    const response = await getDateDetails(repo, tasksWS)(date, userId)
    return httpResponse.ok(response)
  } catch (err) {
    return httpResponse.error(err)
  }
}

exports.updateDate = async event => {
  try {
    event.body = JSON.parse(event.body)
    const data = event.body.data
    const date = event.pathParameters.date
    const userId = event.headers.userId

    const response = await updateDate(repo)(data, date, userId)
    return httpResponse.ok(response)
  } catch (err) {
    return httpResponse.error(err)
  }
}

exports.addTaskToDate = async event => {
  try {
    event.body = JSON.parse(event.body)
    const taskId = event.body.data.taskId
    const date = event.pathParameters.date
    const userId = event.headers.userId

    const response = await addTaskToDate(repo, tasksWS)(taskId, date, userId)
    return httpResponse.ok(response)
  } catch (err) {
    return httpResponse.error(err)
  }
}

exports.getDates = async event => {
  try {
    const userId = event.headers.userId

    const response = await getDates(repo)(userId)
    return httpResponse.ok(response)
  } catch (err) {
    return httpResponse.error(err)
  }
}
