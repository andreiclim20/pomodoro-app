const AWS = require('aws-sdk')
const ddb = new AWS.DynamoDB.DocumentClient()
const _ = require('lodash')

const dbMapper = require('./dbMapper')

const TableName = process.env.TABLE_NAME

exports.getDateDetails = async (Dates, UserId) => {
  const params = {
    TableName,
    Key: { Dates, UserId }
  }

  try {
    const response = await ddb.get(params).promise()

    console.log(`[REPO] GET DETAILS SUCCEEDED FOR ${Dates} AND USER ${UserId}`, response)
    return dbMapper.toCamel(response.Item.Tasks)
  } catch (err) {
    console.log(`[REPO] GET DATE FAILED FOR ${Dates} WITH ERROR ${err}`)
    throw err
  }
}

exports.updateDate = async (data, Dates, UserId) => {
  let UpdateExpression = []
  let ExpressionAttributeNames = {}
  let ExpressionAttributeValues = {}

  Object.keys(data).map(key => {
    UpdateExpression.push(`#${key} = :${key}`)
    ExpressionAttributeNames[`#${key}`] = _.upperFirst(key)
    ExpressionAttributeValues[`:${key}`] = data[key]
  })
  UpdateExpression = `SET ${UpdateExpression.join(',')}`

  const params = {
    TableName,
    Key: { Dates, UserId },
    UpdateExpression,
    ExpressionAttributeNames,
    ExpressionAttributeValues
  }

  try {
    await ddb.update(params).promise()

    console.log(`[REPO] UPDATE DATE SUCCEDED FOR ${Dates} with ${data}`)
    return { data, Dates }
  } catch (err) {
    console.log(`[REPO] UPDATE DATE FAILED FOR ${Dates} with data: ${data}`)
    throw err
  }
}

exports.addTaskToDate = async (taskId, Dates, UserId) => {
  const params = {
    TableName,
    Key: { Dates, UserId },
    UpdateExpression: 'SET Tasks = list_append (Tasks, :id)',
    ExpressionAttributeValues: { ':id': [taskId] }
  }

  try {
    await ddb.update(params).promise()

    console.log(`[REPO] ADD TASK SUCCEEDED TO ${Dates} WITH ${taskId} FOR USER ${UserId}`)
    return { taskId }
  } catch (err) {
    if (err.code === 'ValidationException' && err.statusCode === 400) {
      return createInitialField(taskId, Dates, UserId)
    }
    console.log(`[REPO] ADD TASK FAILED FOR ${Dates} WITH ERROR: `, err)
    throw err
  }
}

exports.getDates = async userId => {
  const params = {
    TableName,
    KeyConditionExpression: 'UserId = :id',
    ExpressionAttributeValues: {
      ':id': userId
    },
    ProjectionExpression: 'Dates',
    Select: 'SPECIFIC_ATTRIBUTES'
  }

  try {
    const response = await ddb.query(params).promise()

    console.log(`[REPO] GET DATES SUCCEEDED FOR ${userId} with data: `, response)
    return response.Items
  } catch (err) {
    console.log(`[REPO] GET DATES FAILED FOR ${userId} WITH ERROR: `, err)
    throw err
  }
}

const createInitialField = async (taskId, Dates, UserId) => {
  let params = {
    TableName,
    Item: {
      Tasks: [taskId],
      Dates,
      UserId
    }
  }

  try {
    await ddb.put(params).promise()

    console.log(`[REPO] CREATE INITIAL FIELD SUCCEEDED FOR ${UserId} FOR TASK ${taskId} ON ${Dates}`)
    return { taskId }
  } catch (err) {
    console.log(`[REPO] CREATE INITIAL FIELD FAILED FOR USER ${UserId} FOR TASK ${taskId} ON ${Dates}`)
    throw err
  }
}
