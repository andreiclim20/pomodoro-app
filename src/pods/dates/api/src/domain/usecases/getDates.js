exports.default = repo => async userId => {
  try {
    const response = await repo.getDates(userId)

    console.log('[USECASES] GET DATES SUCCEDED FOR ', userId)
    return response
  } catch (err) {
    console.log(`[USECASES[ GET DATES FAILED FOR ${userId} WITH ERROR: `, err)
    throw err
  }
}
