exports.default = (repo, tasksWS) => async (taskId, date, userId) => {
  try {
    const result = await repo.addTaskToDate(taskId, date, userId)
    await tasksWS.markAsAssigned(taskId, userId)

    console.log('[USECASES] ADD TASK SUCCEEDED FOR ', taskId, date, userId)
    return result
  } catch (err) {
    console.log('[USECASES] ADD TASK FAILED WITH ERROR ', err)
    throw err
  }
}
