exports.default = repo => async (data, date, userId) => {
  try {
    const result = await repo.updateDate(data, date, userId)

    console.log(`[USECASE] UPDATE DATE SUCCEEDED FOR ${date} FOR USER ${userId} WITH `, data)
    return result
  } catch (err) {
    console.log(`[USECASE] UPDATE DATE ${date} FAILED FOR USER ${userId} WITH ERRPOR: `, err)
    throw err
  }
}
