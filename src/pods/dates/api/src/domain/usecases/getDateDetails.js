exports.default = (repo, tasksWS) => async (date, userId) => {
  try {
    let response = []
    const tasksId = await repo.getDateDetails(date, userId)

    if (tasksId) {
      if (tasksId.length > 0) response = await tasksWS.getTasksDetails(tasksId, userId)
    }

    console.log(`[USECARES] GET DETAILS FOR DATE ${date} SUCCCEDED`)
    return response
  } catch (err) {
    console.log(`[USECARES] GET DETAILS FOR DATE ${date} FAILED WITH ERROR: `, err)
    throw err
  }
}
