const axios = require('axios')
const TASKS_WS = process.env.TASKS_WS

exports.markAsAssigned = async (taskId, userId) => {
  console.log('markAs', taskId)
  try {
    const config = { headers: { userId } }
    const data = { data: { isAssigned: 'true' } }

    await axios.put(`${TASKS_WS}/tasks/${taskId}`, data, config)
  } catch (err) {
    console.log('[TASK WEB SERVICE] FAIL TO MARK TASK AS ASSIGN', taskId)
    throw err
  }
}

exports.getTasksDetails = async (tasksId, userId) => {
  try {
    const config = { headers: { userId } }

    const response = await axios.get(`${TASKS_WS}/tasks?ids=${tasksId.join(',')}`, config)
    return response.data
  } catch (err) {
    console.log('[TASK WEB SERVICE] FAIL TO GET TASKS INFO FOR', tasksId)
    throw err
  }
}
