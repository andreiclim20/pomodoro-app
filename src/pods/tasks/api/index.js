const httpResponse = require('./src/web/httpResponse')
const repo = require('./src/domain/respositories/tasks')

const createTask = require('./src/domain/usecases/createTask').default
const getTasks = require('./src/domain/usecases/getTasks').default
const deleteTask = require('./src/domain/usecases/deleteTask').default
const getTask = require('./src/domain/usecases/getTask').default
const updateTask = require('./src/domain/usecases/updateTask').default

exports.createTask = async event => {
  event.body = JSON.parse(event.body)
  const userId = event.headers.userId
  const data = event.body.data

  try {
    const response = await createTask(repo)(data, userId)

    return httpResponse.ok(response)
  } catch (err) {
    return httpResponse.error(err)
  }
}

exports.getTasks = async event => {
  const userId = event.headers.userId
  const tasksId = event.queryStringParameters ? event.queryStringParameters.ids.split(',') : undefined

  try {
    const response = await getTasks(repo)(userId, tasksId)
    return httpResponse.ok(response)
  } catch (err) {
    return httpResponse.error(err)
  }
}

exports.deleteTask = async event => {
  const userId = event.headers.userId
  const taskId = event.pathParameters.id

  try {
    const response = await deleteTask(repo)(userId, taskId)

    return httpResponse.ok(response)
  } catch (err) {
    return httpResponse.error(err)
  }
}

exports.getTask = async event => {
  const userId = event.headers.userId
  const taskId = event.pathParameters.id

  try {
    const response = await getTask(repo)(userId, taskId)

    return httpResponse.ok(response)
  } catch (err) {
    return httpResponse.error(err)
  }
}

exports.updateTask = async event => {
  event.body = JSON.parse(event.body)
  const userId = event.headers.userId
  const taskId = event.pathParameters.id
  const data = event.body.data

  try {
    const response = await updateTask(repo)(userId, taskId, data)

    return httpResponse.ok(response)
  } catch (err) {
    return httpResponse.error(err)
  }
}
