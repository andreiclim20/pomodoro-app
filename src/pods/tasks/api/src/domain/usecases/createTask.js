const shortId = require('shortid')

exports.default = repo => async (data, userId) => {
  try {
    checkMandadoryFields(data)
    data['TaskId'] = shortId.generate()
    const response = await repo.createTask(data, userId)

    console.log('[USECASES] CREATE TASK SUCCEEDED, ID OF THE TASK CREATED:', data.TaskId)
    return response
  } catch (err) {
    console.log('[USECASES] CREATE TASK FAILED, ID OF THE TASK CREATED:', data.TaskId)
    throw err
  }
}

const checkMandadoryFields = data => {
  if (!data.hasOwnProperty('title')) {
    throw Error('Title is required.')
  }
}
