exports.default = repo => async (userId, taskId, data) => {
  try {
    const response = await repo.updateTask(userId, taskId, data)

    console.log('[USECASES] UPDATE TASK SUCCEEDED, ID OF THE TASK UPDATED:', taskId)
    return response
  } catch (err) {
    console.log('[USECASES] UPDATE TASK FAILED WITH ERROR: ', err)
    throw err
  }
}
