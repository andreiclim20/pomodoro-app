exports.default = repo => async (userId, tasksId) => {
  try {
    const result = await repo.getTasks(userId, tasksId)

    console.log(`[USECASE] GET TASKS SUCCEDDED FOR USER ${userId}`)
    return result
  } catch (err) {
    console.log('[USECASE] GET TASKS FAILED WITH ERROR: ', err)
    throw err
  }
}
