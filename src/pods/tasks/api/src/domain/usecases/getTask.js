exports.default = repo => async (userId, taskId) => {
  try {
    const result = await repo.getTask(userId, taskId)

    console.log('[USECASES] GET TASK SUCCEEDED, ID OF THE TASK:', taskId)
    return result
  } catch (err) {
    console.log('[USECASE] GET TASK FAILED WITH ERROR: ', err)
    throw err
  }
}
