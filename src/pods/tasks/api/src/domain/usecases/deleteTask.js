exports.default = repo => async (userId, taskId) => {
  try {
    const result = await repo.deleteTask(userId, taskId)

    console.log('[USECASES] DELETE TASK SUCCEEDED, ID OF THE TASK DELETED:', taskId)
    return result
  } catch (err) {
    console.log('[USECASE] DELETE TASK FAILED WITH ERROR: ', err)
    throw err
  }
}
