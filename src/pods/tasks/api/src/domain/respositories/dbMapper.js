
const _ = require('lodash')

const toCamel = obj => {
  var newObj, origKey, newKey, value
  if (obj instanceof Array) {
    return obj.map(value => {
      if (typeof value === 'object') {
        value = toCamel(value)
      }
      return value
    })
  } else {
    newObj = {}
    for (origKey in obj) {
      if (obj.hasOwnProperty(origKey)) {
        newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey)
          .toString()
        value = obj[origKey]
        if (
          value instanceof Array ||
          (value !== null && value.constructor === Object)
        ) {
          value = toCamel(value)
        }
        newObj[newKey] = value
      }
    }
  }
  return newObj
}

const toDynamo = obj => {
  var newObj, origKey, newKey, value
  if (obj instanceof Array) {
    return obj.map(value => {
      if (typeof value === 'object') {
        value = toDynamo(value)
      }
      return value
    })
  } else {
    newObj = {}
    for (origKey in obj) {
      if (obj.hasOwnProperty(origKey)) {
        newKey = _.upperFirst(_.camelCase(origKey))
        value = obj[origKey]
        if (value instanceof Array || (value !== null && value.constructor === Object)) {
          value = toDynamo(value)
        }
        newObj[newKey] = value
      }
    }
  }
  return newObj
}

exports.toCamel = toCamel
exports.toDynamo = toDynamo
