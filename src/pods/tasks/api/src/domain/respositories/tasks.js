const AWS = require('aws-sdk')
const ddb = new AWS.DynamoDB.DocumentClient()
const upperFirst = require('lodash').upperFirst
const dbMapper = require('./dbMapper')

const TableName = process.env.TABLE_NAME

exports.createTask = async (data, UserId) => {
  data = dbMapper.toDynamo(data)
  const paramsCreate = {
    TableName,
    Item: { UserId, ...data }
  }
  const paramsScan = {
    TableName,
    FilterExpression: 'UserId = :userId AND attribute_not_exists(IsAssigned)',
    ExpressionAttributeValues: { ':userId': UserId }
  }

  try {
    await ddb.put(paramsCreate).promise()
    const result = await ddb.scan(paramsScan).promise()

    console.log('[REPO] CREATE TASK SUCCEEDED, ID OF THE TASK CREATED: ', data.TaskId)
    return dbMapper.toCamel(result.Items)
  } catch (err) {
    console.log('[REPO] CREATE TASK FAILED WITH ERROR: ', err)
    throw err
  }
}

exports.getTasks = async (userId, tasksId) => {
  let ExpressionAttributeValues = { ':userId': userId }
  let params = { TableName }

  if (tasksId) {
    tasksId.forEach((value, index) => {
      let taskKey = ':taskvalue' + index
      ExpressionAttributeValues[taskKey] = value
    })
    params = Object.assign(params, {
      ExpressionAttributeValues,
      FilterExpression: `UserId = :userId AND TaskId IN (${Object.keys(ExpressionAttributeValues)})`
    })
  } else {
    params = {
      ...params,
      FilterExpression: 'UserId = :userId AND attribute_not_exists(IsAssigned)',
      ExpressionAttributeValues: { ':userId': userId }
    }
  }

  try {
    const result = await ddb.scan(params).promise()

    console.log(`[REPO] GET TASKS SUCCEDED FOR USER ${userId}`)
    return dbMapper.toCamel(result.Items)
  } catch (err) {
    console.log('[REPO] GET TASKS FAILED WITH ERROR: ', err)
    throw err
  }
}

exports.deleteTask = async (UserId, TaskId) => {
  const paramsDelete = {
    TableName,
    Key: { UserId, TaskId }
  }
  const paramsScan = {
    TableName,
    FilterExpression: 'UserId = :userId AND attribute_not_exists(IsAssigned)',
    ExpressionAttributeValues: { ':userId': UserId }
  }

  try {
    await ddb.delete(paramsDelete).promise()
    const result = await ddb.scan(paramsScan).promise()

    console.log('[REPO] DELETE TASK SUCCEEDED, ID OF THE TASK DELETED: ', TaskId)
    return dbMapper.toCamel(result.Items)
  } catch (err) {
    console.log('[REPO] DELETE TASK FAILED WITH ERROR: ', err)
    throw err
  }
}

exports.getTask = async (UserId, TaskId) => {
  const params = {
    TableName,
    Key: { UserId, TaskId }
  }

  try {
    const result = await ddb.get(params).promise()

    console.log('[REPO] GET TASK SUCCEEDED, ID OF THE TASK: ', TaskId)
    return dbMapper.toCamel(result.Item)
  } catch (err) {
    console.log('[REPO] GET TASK FAILED WITH ERROR: ', err)
    throw err
  }
}

exports.updateTask = async (UserId, TaskId, data) => {
  let UpdateExpression = []
  let ExpressionAttributeNames = {}
  let ExpressionAttributeValues = {}

  Object.keys(data).map(key => {
    UpdateExpression.push(`#${key} = :${key}`)
    ExpressionAttributeNames[`#${key}`] = upperFirst(key)
    ExpressionAttributeValues[`:${key}`] = data[key]
  })
  UpdateExpression = `SET ${UpdateExpression.join(',')}`

  const params = {
    TableName,
    Key: { UserId, TaskId },
    UpdateExpression,
    ExpressionAttributeNames,
    ExpressionAttributeValues
  }

  try {
    await ddb.update(params).promise()
    console.log(`[REPO] UPDATE TASK SUCCEDED FOR TASK WITH ID ${TaskId} WITH ${data}`)
    return { data }
  } catch (err) {
    console.log(`[REPO] UPDATE TASK FAILED FOR TASK WITH ID ${TaskId}`)
    throw err
  }
}
