const AWS = require('aws-sdk')
const ddb = new AWS.DynamoDB.DocumentClient()
const TableName = process.env.TABLE_NAME

exports.singUpAutoConfirm = async event => {
  event.response.autoConfirmUser = true
  return event
}

exports.saveUserDetails = async event => {
  const params = {
    TableName,
    Item: {
      UserId: event.request.userAttributes.sub,
      PomodoroLength: 25,
      BreakLength: 5
    }

  }
  try {
    await ddb.put(params).promise()
    console.log('USER DETAILS SUCCESFULLY SAVED')
    return event
  } catch (err) {
    console.log('USER DETAILS FAILED TO SAVE WITH ERR', err)
    throw err
  }
}
