const httpResponse = require('./src/web/httpResponse')
const repo = require('./src/domain/repositories/profile')

const getProfileSettings = require('./src/domain/usecases/getProfileSettings').default
const updateProfileSettings = require('./src/domain/usecases/updateProfileSettings').default

exports.getProfileSettings = async event => {
  try {
    const userId = event.headers.userId

    const response = await getProfileSettings(repo)(userId)
    return httpResponse.ok(response)
  } catch (err) {
    return httpResponse.error(err)
  }
}

exports.updateProfileSettings = async event => {
  try {
    event.body = JSON.parse(event.body)
    const userId = event.headers.userId
    const updatedSettings = event.body.data

    await updateProfileSettings(repo)(userId, updatedSettings)
    return httpResponse.ok()
  } catch (err) {
    return httpResponse.error(err)
  }
}
