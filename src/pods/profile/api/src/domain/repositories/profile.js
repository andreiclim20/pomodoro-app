const AWS = require('aws-sdk')
const ddb = new AWS.DynamoDB.DocumentClient()
const upperFirst = require('lodash').upperFirst

const dbMapper = require('./dbMapper')

const TableName = process.env.TABLE_NAME

exports.getProfileSettings = async UserId => {
  const params = {
    TableName,
    Key: { UserId }
  }

  try {
    const response = await ddb.get(params).promise()

    console.log(`[REPO] GET PROFILE SETTINGS SUCCEEDED FOR ${UserId}`)
    return dbMapper.toCamel(response.Item)
  } catch (err) {
    console.log(`[REPO] GET PROFILE SETTINGS FAILED FOR ${UserId} WITH ERR ${err}`)
    throw err
  }
}

exports.updateProfileSettings = async (UserId, updatedSettings) => {
  let UpdateExpression = []
  let ExpressionAttributeNames = {}
  let ExpressionAttributeValues = {}

  Object.keys(updatedSettings).map(key => {
    UpdateExpression.push(`#${key} = :${key}`)
    ExpressionAttributeNames[`#${key}`] = upperFirst(key)
    ExpressionAttributeValues[`:${key}`] = updatedSettings[key]
  })

  UpdateExpression = `SET ${UpdateExpression.join(',')}`

  const params = {
    TableName,
    Key: { UserId },
    UpdateExpression,
    ExpressionAttributeNames,
    ExpressionAttributeValues
  }

  console.log('PARAMS', params)
  try {
    await ddb.update(params).promise()
  } catch (err) {
    throw err
  }
}
