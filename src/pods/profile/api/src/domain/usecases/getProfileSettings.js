exports.default = repo => async userId => {
  try {
    const response = await repo.getProfileSettings(userId)

    console.log(`[USECASES] GET PROFILE SETTINGS SUCCEEDED FOR USER ${userId}`)
    return response
  } catch (err) {
    console.log(`[USECASES] GET PROFILE SETTINGS FAILED FOR USER ${userId} WITH ERROR`, err)
    throw err
  }
}
