exports.default = repo => async (userId, updatedSettings) => {
  try {
    await repo.updateProfileSettings(userId, updatedSettings)

    console.log(`[USECASE] UPDATE PROFILE SETTING FOR USER ${userId} SUCCEEDED`)
  } catch (err) {
    console.log(`[USECASE] UPDATE PROFILE SETTING FOR USER ${userId} FAILED`)
    throw err
  }
}
