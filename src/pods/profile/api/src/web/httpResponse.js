exports.ok = body => createResponse('200', body)
exports.error = err => createResponse('500', { message: err.message, error: `Look what you've done! You broke it...` })

const createResponse = (statusCode, body) => ({
  statusCode,
  body: JSON.stringify(body),
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  },
  isBase64Encoded: false
})
