import { takeLatest, put, all, call, select } from 'redux-saga/effects'

import {
  FETCH_TO_DOS,
  START_POST_TO_DO,
  FETCH_TODAY_TODOS,
  PUT_TASK_TO_TODAY,
  REMOVE_TASK,
  SET_POMODORO_LENGTH,
  GET_POMODORO_LENGTH
} from '../constants/constants'

import {
  fetchGetToDosSuccess,
  fetchGetToDosError,
  postToDoSuccess,
  postToDoError,
  fetchTodayTodos,
  fetchTodayTodosSuccess,
  fetchTodayTodosError,
  putTaskToToday,
  putTaskToTodaySuccess,
  putTaskToTodayError,
  deleteTaskSuccess,
  deleteTaskError,
  setPomodoroLengthSuccess,
  setPomodoroLengthError,
  getPomodoroLengthSuccess,
  getPomodoroLengthError
} from '../actions/actions'

import {
  getAuthenticatedUserID
} from './selectors'

import { 
  postToDoCall,
  getToDosCall,
  moveTaskToTodayCall,
  getTodayTodosCall,
  removeTaskCall,
  putPomodoroLengthCall,
  getPomodoroLengthCall
} from './dataservice'

const _getCurrentDate = () => {
  let today = new Date()
  let day = today.getDate()
  let month = today.getMonth() + 1
  let year = 2019
  let result = (day < 10 ? '0' + day : day) + '-' + (month < 10 ? '0' + month : month) + '-' + year
  console.log(result)
  return result
}

function * fetchToDos () {
  try {
    const userId = yield select(getAuthenticatedUserID)
    const response = yield call(getToDosCall, userId)
    console.log('FETCH TODOS SAGAA:', response.data)
    yield put(fetchGetToDosSuccess(response.data))
  } catch (err) {
    console.log('Error caught in get to dos SAGA:', err)
    yield put(fetchGetToDosError(err))
  }
}

function * postToDo (action) {
  const userId = yield select(getAuthenticatedUserID)
  const { toDo } = action.payload
  try {
    const response = yield call(postToDoCall, userId, toDo)
    console.log('responsePostToDo:', response)
    yield put(postToDoSuccess(response))
  } catch (err) {
    console.log('Error caught in post to do SAGA:', err)
    yield put(postToDoError(err))
  }
}

function * fetchTodayList () {
  const userId = yield select(getAuthenticatedUserID)
  try {
    const response = yield call(getTodayTodosCall, _getCurrentDate(), userId)
    console.log('FETCH TODAY LIST RESPONSE:', response)
    yield put(fetchTodayTodosSuccess(response))
  } catch (err) {
    console.log('Error caught in fetchTodayList:', err)
  }
}

function * moveTaskToToday (action) {
  const userId = yield select(getAuthenticatedUserID)
  const { taskId } = action.payload
  try {
    const response = yield call(moveTaskToTodayCall, _getCurrentDate(), taskId, userId)
    console.log('[RESPONSE][MOVE TASK TO TODAY]', response)
    yield put(putTaskToTodaySuccess(response.taskId))
    yield call(fetchTodayList)
  } catch (err) {
    yield put(putTaskToTodayError)
    console.log('Error caught in moveTaskToTodaySaga:', err)
  }
}

function * removeTask (action) {
  const userId = yield select(getAuthenticatedUserID)
  const { taskId } = action.payload
  try {
    const response = yield call(removeTaskCall, taskId, userId)
    console.log('responseRemoveTaskCall:', response)
    yield put(deleteTaskSuccess(response))
  } catch (err) {
    console.log('Error caught in REMOVE_TASK Saga:', err)
    yield put(deleteTaskError(err))
  }
}

function * updatePomodoroLength (action) {
  const userId = yield select(getAuthenticatedUserID)
  const { newLength } = action.payload
  console.log('updatePomodoroLengthSaga:', newLength)
  try {
    const response = yield call(putPomodoroLengthCall, userId, newLength)
    console.log('responsePutPomodoroLengthCall:', response)
    yield put(setPomodoroLengthSuccess(response))
    yield call(getPomodoroLength)
  } catch (err) {
    console.log('Error caught in SET_POMODORO_LENGTH Saga:', err)
    yield put(setPomodoroLengthError(err))
  }
}

function * getPomodoroLength () {
  const userId = yield select(getAuthenticatedUserID)
  try {
    const response = yield call(getPomodoroLengthCall, userId)
    console.log('responseGetPomodoroLengthCall:', response)
    yield put(getPomodoroLengthSuccess(response.data))
  } catch (err) {
    console.log('Error caught in GET_POMODORO_LENGTH Saga:', err)
    yield put(getPomodoroLengthError(err))
  }
}

export default function * mainSaga () {
  yield all([
    yield takeLatest(FETCH_TO_DOS, fetchToDos),
    yield takeLatest(START_POST_TO_DO, postToDo),
    yield takeLatest(FETCH_TODAY_TODOS, fetchTodayList),
    yield takeLatest(PUT_TASK_TO_TODAY, moveTaskToToday),
    yield takeLatest(REMOVE_TASK, removeTask),
    yield takeLatest(SET_POMODORO_LENGTH, updatePomodoroLength),
    yield takeLatest(GET_POMODORO_LENGTH, getPomodoroLength)
  ])
}
