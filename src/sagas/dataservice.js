import { API } from 'aws-amplify'

export const postToDoCall = (userId, toDo) => {
  console.log('toDOCall:', toDo)
  return API.post('todos', '/tasks', {
    headers: {
      User: userId
    },
    body: {
      data: {
        ...toDo
      },
      response: true
    }
  })
}

export const getToDosCall = userId => {
  return API.get('todos', '/tasks', {
    headers: {
      User: userId
    },
    response: true
  })
}

export const moveTaskToTodayCall = (date, taskId, userId) => {
  console.log('MoveCall:', taskId, userId)
  return API.put('todos', `/dates/${date}/task`, {
    headers: {
      User: userId
    },
    body: {
      data: { taskId },
      response: true
    }
  })
}

export const getTodayTodosCall = (date, userId) => {
  return API.get(
    'todos',
    `/dates/${date}`,
    { 
      headers: { User: userId } 
    }
  )
}

export const removeTaskCall = (taskId, userId) => {
  console.log('RemooveCall:', taskId, userId)
  return API.del('todos', `/tasks/${taskId}`, {
    headers: {
      User: userId
    },
    body: {
      data: { taskId },
      response: true
    }
  })
}

export const putPomodoroLengthCall = (userId, newLengths) => {
  return API.put('todos', `/settings`, {
    headers: {
      User: userId
    },
    body: {
      data: { ...newLengths },
      response: true
    }
  })
}

export const getPomodoroLengthCall = userId => {
  return API.get('todos', `/settings`, {
    headers: {
      User: userId
    },
    response: true
  })
}
