import { AppRegistry } from 'react-native'
import App from './App'
import { name as appName } from './app.json'

import Amplify, { Auth } from 'aws-amplify'
import aws_config from './src/config/aws-config'
Amplify.configure(aws_config)

AppRegistry.registerComponent(appName, () => App)
